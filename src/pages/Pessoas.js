import PessoaCard from "../components/pessoaCard/PessoaCard";
import { useState, useEffect, useRef } from "react";
import { useSearchParams } from "react-router-dom";

import arquivo from "../arquivo.json";


const Pessoas = () => {
    const [dados, setDados] = useState(arquivo);
    //const [busca, setBusca] = useState();
    const [parametros, setParametros] = useSearchParams();
    const entrada = useRef();


    const filtra = (dados) => {
        const parametro = parametros.get("busca");

        if (!parametro) {
            return dados;

        } else {
            const filtrados = dados.filter(
                (e) => e.nome.toLowerCase().includes(parametro.toLocaleLowerCase()) || e.cpf.toLowerCase().includes(parametro.toLocaleLowerCase())
            );
            return filtrados;
        }
       
    }


    useEffect(() => {
        const dadosFiltrados = filtra(arquivo);     
        setDados(dadosFiltrados);
    });

    return (
        <div>
            <form onSubmit={(e) => {
                e.preventDefault();
                setParametros({"busca": `${entrada.current.value}`})
                }}>
                <input 
                    type="text"
                    ref={entrada}
                    />
                <button type='submit'>Pesquisar</button>
            </form>
        {
            dados.map( (p, ind) => (
                <PessoaCard
                    key={ ind } 
                    imagem={p.imagem}
                    nome={p.nome}
                    cpf={p.cpf}
                />
            ))
        }
        
        </div>
    )
}

export default Pessoas;