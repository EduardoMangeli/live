import { Link } from "react-router-dom";
import Primeiro from "../components/primeiro/Primeiro";

import arquivo from "../arquivo.json";

const Exemplo = () => {
    return (
        <div>
            <h1>Minha página</h1>
            {
                arquivo.map( ( pessoa, chave ) => (
                    <Primeiro 
                        key={ chave }
                        nome={ pessoa.nome }
                        cpf={ pessoa.cpf }
                    />
                ))
            }
            <Link to="/">Home</Link>
        </div>
    );
}

export default Exemplo;