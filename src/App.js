import './App.css';

import { Link } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Link to="/exe">Minha página de exemplo</Link>
    </div>
  );
}

export default App;
