import { P, Div } from "./Style";

const Primeiro = ( props ) => {
    return (
        <Div>
            <P>{ props.nome }</P>
            { props.cpf }
        </Div>
    );
}

export default Primeiro;