import styled from 'styled-components';

const Div = styled.div`
    color: green;
    background-color: bisque;
`;

const P = styled.p`
    font-style: italic;
    text-transform: capitalize;
`; 

export { Div, P };