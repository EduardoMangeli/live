import styled from "styled-components";

const Card = styled.div`
    width: 20em;
    border-width: 1px;
    background-color: aquamarine;
    display: flex;
    flex-flow: column;

    &:hover{
        cursor: crosshair;
        transform: scale(1.05);
    }

    @media screen and (min-width: 1024px){
        flex-flow: row;
    }
`;

const Img = styled.img`
    width: 5em;
    border-radius: 50%;
    margin: auto;

    @media screen and (min-width: 1024px){
        margin: 3px;
    }
`;

const Textos = styled.div`
    /*font-family: Arial, Helvetica, sans-serif;*/
    background-color: crimson;
    flex-grow: 1;
    padding: .5em;
`;

const Nome = styled.p`
    font-family: 'Barlow Condensed', sans-serif;;
    text-transform: uppercase;
    font-weight: 400;
`;

const Cpf = styled.p`
    font-weight: 200;
`;

export { Card, Img, Textos, Nome, Cpf }


